<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt_LT">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qml/MainWindow.qml" line="13"/>
        <source>EasyDebug</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowBleCentralPageController</name>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="35"/>
        <source>BLE central settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="39"/>
        <source>Device list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="46"/>
        <source>filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="55"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="64"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="71"/>
        <source>Characteristics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="78"/>
        <source>Writting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="82"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="82"/>
        <source>Write without Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="94"/>
        <source>Notify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="94"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowBleCentralPageController.qml" line="110"/>
        <source>Unnotify</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowInfoPage</name>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="12"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="18"/>
        <source>Report a bug:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="24"/>
        <source>Build info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="30"/>
        <source>User QQ Group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="40"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowInfoPage.qml" line="45"/>
        <source>Copyright information:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowSerialPortPageController</name>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="43"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="44"/>
        <source><byte value="xd"/></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="45"/>
        <source>
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="46"/>
        <source><byte value="xd"/>
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="47"/>
        <source>
<byte value="xd"/></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="55"/>
        <source>Serial port settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="60"/>
        <source>Port names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="80"/>
        <source>Baud rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="115"/>
        <source>Stop bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="142"/>
        <source>Data bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="170"/>
        <source>Parity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="179"/>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="208"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="180"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="181"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="182"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="183"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="199"/>
        <source>Flow control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="209"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="210"/>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="228"/>
        <source>Custom baud rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="242"/>
        <source>Ignore busy device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSerialPortPageController.qml" line="258"/>
        <source>Auto update port names</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowSettingsPage</name>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="14"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="19"/>
        <source>High dpi scale factor rounding policy (need to reboot the app)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="27"/>
        <source>.5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="27"/>
        <source>Round up for .5 and above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="28"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="28"/>
        <source>Always round up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="29"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="29"/>
        <source>Always round down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="30"/>
        <source>.75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="30"/>
        <source>Round up for .75 and above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="31"/>
        <source>Don&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="31"/>
        <source>Don&apos;t round.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="50"/>
        <source>UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="53"/>
        <source>Classical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="53"/>
        <source>Modern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="68"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="81"/>
        <source>Font Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="91"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="95"/>
        <source>Application theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="102"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="103"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="104"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="111"/>
        <source>Accent color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="112"/>
        <source>Primary color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="122"/>
        <source>Reset accent color and primary to default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowSettingsPage.qml" line="125"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowTcpClientPageController</name>
    <message>
        <location filename="../../qml/MainWindowTcpClientPageController.qml" line="18"/>
        <source>TCP client settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowTcpServerPageController</name>
    <message>
        <location filename="../../qml/MainWindowTcpServerPageController.qml" line="18"/>
        <source>TCP server settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowToolBar</name>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="41"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="42"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="74"/>
        <source>SerialPort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="43"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="75"/>
        <source>UDP Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="44"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="76"/>
        <source>UDP Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="45"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="77"/>
        <source>TCP Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="46"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="78"/>
        <source>TCP Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="47"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="79"/>
        <source>WebSocket Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainWindowToolBar.qml" line="48"/>
        <location filename="../../qml/MainWindowToolBar.qml" line="80"/>
        <source>WebSocket Server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowUdpClientPageController</name>
    <message>
        <location filename="../../qml/MainWindowUdpClientPageController.qml" line="18"/>
        <source>UDP client settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowUdpServerPageController</name>
    <message>
        <location filename="../../qml/MainWindowUdpServerPageController.qml" line="18"/>
        <source>UDP server settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowWebSocketClientPageController</name>
    <message>
        <location filename="../../qml/MainWindowWebSocketClientPageController.qml" line="20"/>
        <source>WebSocket client settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowWebSocketServerPageController</name>
    <message>
        <location filename="../../qml/MainWindowWebSocketServerPageController.qml" line="19"/>
        <source>WebSocket server settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/SAKApplication.cc" line="56"/>
        <source>Initializing main window...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtCryptographicHashCalculator</name>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/QtCryptographicHashCalculator.cc" line="76"/>
        <source>Calculating finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKAffixesComboBox</name>
    <message>
        <location filename="../../src/commonui/SAKAffixesComboBox.cc" line="16"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKAnalyzerToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="32"/>
        <source>Analyzer parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="40"/>
        <source>Fixed length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="47"/>
        <source>Frame length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="57"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="64"/>
        <source>Max temp bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="96"/>
        <source>Separation mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKAnalyzerToolUi.ui" line="103"/>
        <source>Hex format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKApplication</name>
    <message>
        <location filename="../../src/SAKApplication.cc" line="50"/>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKApplication.cc" line="89"/>
        <source>Finished...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKAssistantsFactory</name>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="41"/>
        <source>CRC Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="42"/>
        <source>File Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="43"/>
        <source>ASCII Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="44"/>
        <source>Float Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="45"/>
        <source>String Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/SAKAssistantsFactory.cc" line="46"/>
        <source>Broadcast Assistant</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKBleCentralToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="34"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="41"/>
        <source>Notify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="48"/>
        <source>Write(R)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="55"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="64"/>
        <source>Characteristic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="81"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="88"/>
        <source>Name filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="98"/>
        <source>Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="108"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKBleCentralToolUi.ui" line="118"/>
        <source>Scan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKCommonDataStructure</name>
    <message>
        <location filename="../../src/common/SAKCommonDataStructure.cc" line="64"/>
        <source>BIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonDataStructure.cc" line="65"/>
        <source>TEXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonDataStructure.cc" line="298"/>
        <location filename="../../src/common/SAKCommonDataStructure.cc" line="303"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKCommonInterface</name>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="160"/>
        <location filename="../../src/common/SAKCommonInterface.cc" line="179"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="162"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="164"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="166"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="168"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="181"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/common/SAKCommonInterface.cc" line="183"/>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKEmitterToolUiEditor</name>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="20"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="40"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="47"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="76"/>
        <source>The CRC start index is start form header, the first byte is 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="96"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="106"/>
        <source>The CRC end index is from tail, the last byte of data is 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="129"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="136"/>
        <source>Start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="143"/>
        <source>Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="150"/>
        <source>CRC enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="157"/>
        <source>Escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="167"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="174"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="181"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="188"/>
        <source>Sufix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="195"/>
        <source>End index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="205"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.ui" line="212"/>
        <source> If the parameters of CRC is set error, all bytes of data will be calculcated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKEmitterToolUiEditor.cc" line="20"/>
        <source>Emitter Item Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKEscComboBox</name>
    <message>
        <location filename="../../qml/common/SAKEscComboBox.qml" line="11"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKEscapeCharacterComboBox</name>
    <message>
        <location filename="../../src/commonui/SAKEscapeCharacterComboBox.cc" line="16"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKFlowControlComboBox</name>
    <message>
        <location filename="../../src/commonui/SAKFlowControlComboBox.cc" line="16"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKFlowControlComboBox.cc" line="17"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKFlowControlComboBox.cc" line="18"/>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKMainWindow</name>
    <message>
        <location filename="../../src/SAKMainWindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="69"/>
        <source>Qt Swiss Army Knife</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="150"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="153"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="169"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="176"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="201"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="224"/>
        <source>Application Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="263"/>
        <source>Main Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="265"/>
        <source>Exit to Sysytem Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="285"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="288"/>
        <source>Clear Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="292"/>
        <source>Open configuration floder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="304"/>
        <source>UI Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="306"/>
        <source>Classical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="313"/>
        <source>Modern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="336"/>
        <source>HDPI Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="339"/>
        <source>Round up for .5 and above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="340"/>
        <source>Always round up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="341"/>
        <source>Always round down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="342"/>
        <source>Round up for .75 and above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="343"/>
        <source>Don&apos;t round.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="399"/>
        <source>&amp;Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="457"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="460"/>
        <location filename="../../src/SAKMainWindow.cc" line="465"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="467"/>
        <source>About Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="472"/>
        <source>Get Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="476"/>
        <source>GitHub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="485"/>
        <source>Gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="493"/>
        <location filename="../../src/SAKMainWindow.cc" line="661"/>
        <source>Release History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="499"/>
        <location filename="../../src/SAKMainWindow.cc" line="681"/>
        <source>QR Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="504"/>
        <source>支持作者</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="510"/>
        <source>&amp;Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="520"/>
        <source>Qt Official Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="524"/>
        <source>Qt Official Blog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="527"/>
        <source>Qt Official Release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="530"/>
        <source>Download SAK from Github</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="533"/>
        <source>Download SAK from Gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="536"/>
        <source>Office Web Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="553"/>
        <source>&amp;Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="563"/>
        <source>Qt SerialPort Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="592"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="594"/>
        <source>Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="595"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="596"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="597"/>
        <source>QQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="598"/>
        <source>QQ Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="599"/>
        <source>Build Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="600"/>
        <source>Gitee Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="602"/>
        <source>Gitbub Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="604"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="605"/>
        <source>Copyright 2018-%1 Qsaker(qsaker@outlook.com). All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="610"/>
        <source>About QSAK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="647"/>
        <source>Reboot application to effective</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="648"/>
        <source>Need to reboot, reboot to effective now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="689"/>
        <source>User QQ Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKMainWindow.cc" line="691"/>
        <source>Qt QQ Group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKMaskerToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKMaskerToolUi.ui" line="32"/>
        <source>Masker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKMaskerToolUi.ui" line="38"/>
        <source>Mask code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKMaskerToolUi.ui" line="52"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKParityComboBox</name>
    <message>
        <location filename="../../src/commonui/SAKParityComboBox.cc" line="16"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKParityComboBox.cc" line="17"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKParityComboBox.cc" line="18"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKParityComboBox.cc" line="19"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKParityComboBox.cc" line="20"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKPrestorerToolUiEditor</name>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="20"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="27"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="47"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="53"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="60"/>
        <source>Suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="67"/>
        <source>Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="77"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="93"/>
        <source>Escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="100"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="107"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="117"/>
        <source>Append CRC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="124"/>
        <source>Start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.ui" line="138"/>
        <source>End index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKPrestorerToolUiEditor.cc" line="20"/>
        <source>Prestorer Item Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKRNComboBox</name>
    <message>
        <location filename="../../qml/common/SAKRNComboBox.qml" line="9"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKResponseOptionComboBox</name>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="16"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="17"/>
        <source>Echo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="18"/>
        <source>Aways</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="19"/>
        <source>InputEqualReference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="20"/>
        <source>InputContainReference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/commonui/SAKResponseOptionComboBox.cc" line="21"/>
        <source>InputDiscontainReference</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKResponseOptionsComboBox</name>
    <message>
        <location filename="../../qml/common/SAKResponseOptionsComboBox.qml" line="12"/>
        <source>Echo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/common/SAKResponseOptionsComboBox.qml" line="13"/>
        <source>Aways</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/common/SAKResponseOptionsComboBox.qml" line="14"/>
        <source>InputEqualReference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/common/SAKResponseOptionsComboBox.qml" line="15"/>
        <source>InputContainReference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/common/SAKResponseOptionsComboBox.qml" line="16"/>
        <source>InputDiscontainReference</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKResponserToolUiEditor</name>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="26"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="46"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="53"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="72"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="273"/>
        <source>End index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="82"/>
        <source>Reference data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="99"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="190"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="106"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="113"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="266"/>
        <source>Suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="120"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="252"/>
        <source>Start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="130"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="140"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="290"/>
        <source>Escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="147"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="228"/>
        <source>Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="154"/>
        <source>Response data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="167"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="174"/>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="214"/>
        <source>Append CRC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="207"/>
        <source>Fromat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="242"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="259"/>
        <source>Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.ui" line="283"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKResponserToolUiEditor.cc" line="20"/>
        <source>Responser Item Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKSerialPortToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="39"/>
        <source>Port name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="65"/>
        <source>Baud rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="72"/>
        <source>Data bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="79"/>
        <source>Stop bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="86"/>
        <source>Parity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSerialPortToolUi.ui" line="93"/>
        <source>Flow control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKSocketClientToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="62"/>
        <source>Client port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="69"/>
        <source>(closed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="76"/>
        <source>Server IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="83"/>
        <source>Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="90"/>
        <source>Context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="107"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="114"/>
        <source>Client IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.ui" line="137"/>
        <source>Specify ip and port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketClientToolUi.cc" line="83"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKSocketServerToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="42"/>
        <source>(closed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="49"/>
        <source>Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="69"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="77"/>
        <source>All clients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="85"/>
        <source>Binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="92"/>
        <source>Server IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="99"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="106"/>
        <source>Specify ip and port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.ui" line="113"/>
        <source>send to client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKSocketServerToolUi.cc" line="89"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKStatisticianToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKStatisticianToolUi.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStatisticianToolUi.ui" line="38"/>
        <source>Frames:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStatisticianToolUi.ui" line="58"/>
        <source>Bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKStorerToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="32"/>
        <source>Storer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="38"/>
        <source>Saving file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="48"/>
        <source>Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="57"/>
        <source>Save time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="64"/>
        <source>Save date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="71"/>
        <source>Save tx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="78"/>
        <source>Save rx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="85"/>
        <source>Save ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.ui" line="92"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.cc" line="86"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKStorerToolUi.cc" line="87"/>
        <source>txt (*.txt); all (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKSystemTrayIcon</name>
    <message>
        <location filename="../../src/SAKSystemTrayIcon.cc" line="19"/>
        <source>Qt Swiss Army Knife</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKSystemTrayIcon.cc" line="22"/>
        <source>Open main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/SAKSystemTrayIcon.cc" line="25"/>
        <source>Exit program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKTableModelToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="32"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="39"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="46"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="53"/>
        <source>Append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="60"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="70"/>
        <source>Visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.ui" line="77"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="237"/>
        <source>Please Select an Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="238"/>
        <source>Please select an tiem first, then try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="263"/>
        <source>Clear Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="264"/>
        <source>The data will be empty from settings file, please confrim the operation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="276"/>
        <source>Delete Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="277"/>
        <source>The data will be delete from settings file, please confrim the operation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="295"/>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="318"/>
        <source>Import data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="297"/>
        <source>JSON (*.json);;All (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKTableModelToolUi.cc" line="320"/>
        <source>JSON (*.json);;All (*); </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolAsciiAssistant</name>
    <message>
        <location filename="../../src/assistants/asciiassistant/src/SAKToolAsciiAssistant.ui" line="14"/>
        <source>Ascii Assistant</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolBoxUi</name>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="63"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="70"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="77"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="84"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="112"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="187"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="91"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="304"/>
        <source>Rx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="98"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="105"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="283"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="132"/>
        <source>MS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="152"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="159"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="166"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="173"/>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="336"/>
        <source>Tx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="180"/>
        <source>Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="201"/>
        <source>Input control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="208"/>
        <source>Output control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="215"/>
        <source>Communication control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="235"/>
        <source>I/O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="277"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="292"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="366"/>
        <source>Emiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="371"/>
        <source>Responser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.ui" line="376"/>
        <source>Prestorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="74"/>
        <source>SerialPort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="76"/>
        <source>UDP Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="78"/>
        <source>UDP Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="80"/>
        <source>TCP Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="82"/>
        <source>TCP Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="84"/>
        <source>WebSocket Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="86"/>
        <source>WebSocket Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="88"/>
        <source>BLE Central</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="281"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUi.cc" line="388"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolBoxUiParameters</name>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="20"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="44"/>
        <source>Communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="76"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="88"/>
        <source>Preprocessing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="103"/>
        <source>Frame suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="110"/>
        <source>Frame prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="117"/>
        <source>Escape character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="140"/>
        <source>CRC settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="148"/>
        <source>Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="167"/>
        <source>Big endian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="187"/>
        <source>End index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="194"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="201"/>
        <source>Start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="231"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.ui" line="256"/>
        <source>Highlighter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.cc" line="24"/>
        <source>Tx analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolboxui/SAKToolBoxUiParameters.cc" line="25"/>
        <source>Rx analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolBroadcastAssistant</name>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="20"/>
        <source>Information output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="26"/>
        <source>Output format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="49"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="62"/>
        <source>Broadcast parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="71"/>
        <source>Broadcast port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="78"/>
        <source>Broadcast suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="94"/>
        <source>Broadcast prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="101"/>
        <source>Broadcast data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="108"/>
        <source>Broadcast interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="115"/>
        <source>Data format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="122"/>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.cc" line="68"/>
        <source>Broadcast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="132"/>
        <source>Broadcast address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.ui" line="139"/>
        <source>55443</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.cc" line="55"/>
        <source>Broadcast Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/broadcastassistant/src/SAKToolBroadcastAssistant.cc" line="67"/>
        <source>Terminate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolCRCAssistant</name>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="14"/>
        <source>CRC Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="20"/>
        <source>Paloy value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="27"/>
        <source>Initial value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="34"/>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="232"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="44"/>
        <source>More information about crc calculation, visit http://www.ip33.com/crc.html(double clicked to visit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="60"/>
        <source>Input reversal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="67"/>
        <source>Output reversal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="87"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="94"/>
        <source>Calculate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="104"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="114"/>
        <source>XOR value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="124"/>
        <source>Height its is on the left and the low bits is on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="157"/>
        <source>HEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="167"/>
        <source>ASCII</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="190"/>
        <source>Result(HEX)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="197"/>
        <source>Parameter model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="214"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="221"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="262"/>
        <source>Paloy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="276"/>
        <source>Input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/crcassistant/src/SAKToolCRCAssistant.ui" line="283"/>
        <source>Result(BIN)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolFileCheckAssistant</name>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="37"/>
        <source>Upper result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="76"/>
        <source>Checked file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="89"/>
        <source>Checked progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="102"/>
        <source>Checked algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="115"/>
        <source>Checked result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="125"/>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.cc" line="176"/>
        <source>Calculate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.ui" line="132"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.cc" line="63"/>
        <source>File Check Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.cc" line="125"/>
        <source>Remaining time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/filecheckassistant/src/SAKToolFileCheckAssistant.cc" line="182"/>
        <source>StopCalculating</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolFloatAssistant</name>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="14"/>
        <source>Float Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="26"/>
        <source>Raw data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="55"/>
        <source>Float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="65"/>
        <source>Double</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="72"/>
        <source>HexRawData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="79"/>
        <source>Big endian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="86"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/floatassistant/src/SAKToolFloatAssistant.ui" line="112"/>
        <source>Cooked data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKToolStringAssistant</name>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="14"/>
        <source>String Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="36"/>
        <source>Raw data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="43"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="50"/>
        <source>Cooked data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="94"/>
        <source>Input format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/assistants/stringassistant/src/SAKToolStringAssistant.ui" line="107"/>
        <source>Output format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAKVelometerToolUi</name>
    <message>
        <location filename="../../src/toolsui/SAKVelometerToolUi.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/toolsui/SAKVelometerToolUi.ui" line="38"/>
        <source>Velometer:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxCommonClient</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="40"/>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="84"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="51"/>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="93"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="62"/>
        <source>Binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="73"/>
        <source>Specify client information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="80"/>
        <source>Server settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonClient.qml" line="102"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxCommonCrcParameters</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonCrcParameters.qml" line="8"/>
        <source>CRC Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonCrcParameters.qml" line="27"/>
        <source>CRC enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonCrcParameters.qml" line="32"/>
        <source>CRC arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonCrcParameters.qml" line="39"/>
        <source>CRC start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonCrcParameters.qml" line="46"/>
        <source>CRC end index</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxCommonServer</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonServer.qml" line="33"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonServer.qml" line="43"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonServer.qml" line="52"/>
        <source>Clients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonServer.qml" line="60"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxCommonTableView</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="103"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="104"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="105"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="106"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="107"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="108"/>
        <source>Append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="158"/>
        <source>Clear Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="159"/>
        <source>The items will be clear, please confirm the operaion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="160"/>
        <source>The items will be clear and it will be remove from the settings file. You can backup the items by &quot;Export&quot; operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="172"/>
        <source>Remove a Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="173"/>
        <source>The item will be removed, please confirm the operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="184"/>
        <source>Please Select an Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxCommonTableView.qml" line="185"/>
        <source>Please select an item fist, the operaion need you to specify an item.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxController</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="81"/>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="125"/>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="190"/>
        <source>Conf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="89"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="90"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="107"/>
        <source>Output settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="114"/>
        <source>Outut format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="130"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="143"/>
        <source>Input settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="150"/>
        <source>Timer sending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="156"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="174"/>
        <source>Input format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxController.qml" line="190"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawer</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawer.qml" line="39"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawer.qml" line="39"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawer.qml" line="39"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerDeviceAnalyzer</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="10"/>
        <source>Analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="31"/>
        <source>Enable frame analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="46"/>
        <source>Fixed frame length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="60"/>
        <source>Frame bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="77"/>
        <source>Separation mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceAnalyzer.qml" line="97"/>
        <source>Max temp bytes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerDeviceMasker</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceMasker.qml" line="10"/>
        <source>Masker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceMasker.qml" line="32"/>
        <source>Enable reading masker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceMasker.qml" line="41"/>
        <source>Enable writing masker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceMasker.qml" line="49"/>
        <source>Reading mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerDeviceMasker.qml" line="63"/>
        <source>Writing mask</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerInputCrc</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="10"/>
        <source>CRC Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="30"/>
        <source>CRC type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="38"/>
        <source>Start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="47"/>
        <source>End index(from tail)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="57"/>
        <source>Append CRC(before suffix)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputCrc.qml" line="65"/>
        <source>The start index is start from header, the first byte is 0. The end index is from tail, the last byte is 0. If parameters are set error, all bytes inputed will be calculated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerInputFileSendding</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="9"/>
        <source>File sending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="22"/>
        <source>Frame length(byte)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="29"/>
        <source>Frame interval(ms)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="36"/>
        <source>SelectFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="40"/>
        <source>StartSending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputFileSendding.qml" line="45"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerInputPreprocessor</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputPreprocessor.qml" line="10"/>
        <source>Data preprocessor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputPreprocessor.qml" line="27"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputPreprocessor.qml" line="35"/>
        <source>Suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerInputPreprocessor.qml" line="43"/>
        <source>Escape character</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerOutputHighlighter</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputHighlighter.qml" line="10"/>
        <source>Highlighter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputHighlighter.qml" line="26"/>
        <source>Key word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputHighlighter.qml" line="35"/>
        <source>Append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputHighlighter.qml" line="39"/>
        <source>Double click to delete key word</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerOutputPreprocessor</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="10"/>
        <source>Preprocessor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="34"/>
        <source>Show read bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="35"/>
        <source>Show written bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="36"/>
        <source>Show date information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="37"/>
        <source>Show time information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="38"/>
        <source>Show ms information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputPreprocessor.qml" line="39"/>
        <source>Show wrap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxDrawerOutputStorer</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="11"/>
        <source>Storer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="31"/>
        <source>Enable data storer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="32"/>
        <source>Save date information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="33"/>
        <source>Save time information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="34"/>
        <source>Save ms information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="60"/>
        <source>Text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="77"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="101"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxDrawerOutputStorer.qml" line="101"/>
        <source>All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxReadWrite</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWrite.qml" line="39"/>
        <source>I/O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWrite.qml" line="39"/>
        <source>Emitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWrite.qml" line="39"/>
        <source>Responser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWrite.qml" line="39"/>
        <source>PresetData</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxReadWriteEmitterPopup</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="30"/>
        <source>Enable the item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="35"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="42"/>
        <source>Item text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="51"/>
        <source>Item escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="59"/>
        <source>Item prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="67"/>
        <source>Item suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="75"/>
        <source>Item interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="83"/>
        <source>Item text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="96"/>
        <source>Append CRC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="101"/>
        <source>CRC arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="108"/>
        <source>CRC start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="115"/>
        <source>CRC end index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="127"/>
        <source>Cancle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteEmitterPopup.qml" line="127"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxReadWriteInputOutput</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="61"/>
        <source>Rx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="62"/>
        <source>Tx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="63"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="64"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="65"/>
        <source>MS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="66"/>
        <source>Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="98"/>
        <source>Output text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="156"/>
        <source>Cycle sending interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="158"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="175"/>
        <source>Input text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteInputOutput.qml" line="239"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxReadWritePrestorerPopup</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="27"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="34"/>
        <source>Item text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="43"/>
        <source>Item escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="51"/>
        <source>Item prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="59"/>
        <source>Item suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="67"/>
        <source>Item text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="80"/>
        <source>Append CRC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="85"/>
        <source>CRC arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="92"/>
        <source>CRC start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="99"/>
        <source>CRC end index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="111"/>
        <source>Cancle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWritePrestorerPopup.qml" line="111"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBoxReadWriteResponserPopup</name>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="57"/>
        <source>Enable the item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="62"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="69"/>
        <source>Response options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="86"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="86"/>
        <source>Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="94"/>
        <source>Text format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="103"/>
        <source>Item escape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="111"/>
        <source>Item prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="119"/>
        <source>Item suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="127"/>
        <source>Item interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="136"/>
        <source>Item text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="149"/>
        <source>Append CRC data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="154"/>
        <source>CRC arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="161"/>
        <source>CRC start index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="168"/>
        <source>CRC end index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="248"/>
        <source>Cancle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/toolbox/ToolBoxReadWriteResponserPopup.qml" line="248"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
