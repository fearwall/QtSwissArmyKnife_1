INCLUDEPATH += $$PWD

FORMS += \
    $$PWD/SAKBaseListWidget.ui

HEADERS += \
    $$PWD/SAKBaseListWidget.hh \
    $$PWD/SAKBaseListWidgetItemWidget.hh \
    $$PWD/SAKDevice.hh \
    $$PWD/SAKObj.hh \
    $$PWD/SAKSigleton.hh

SOURCES += \
    $$PWD/SAKBaseListWidget.cc \
    $$PWD/SAKBaseListWidgetItemWidget.cc \
    $$PWD/SAKDevice.cc \
    $$PWD/SAKObj.cc \
    $$PWD/SAKSigleton.cc
