INCLUDEPATH += src/common

HEADERS += \
    $$PWD/SAKBleScanner.hh \
    $$PWD/SAKCommonCrcInterface.hh \
    $$PWD/SAKCommonDataStructure.hh \
    $$PWD/SAKCommonInterface.hh \
    $$PWD/SAKCrcInterface.hh \
    $$PWD/SAKDataStructure.hh \
    $$PWD/SAKHighlighter.hh \
    $$PWD/SAKInterface.hh \
    $$PWD/SAKNetworkInterfaceScanner.hh \
    $$PWD/SAKObj.hh \
    $$PWD/SAKSerialPortScanner.hh \
    $$PWD/SAKSettings.hh \
    $$PWD/SAKSigleton.hh \
    $$PWD/SAKTableModel.hh \
    $$PWD/SAKTranslator.hh

SOURCES += \
    $$PWD/SAKBleScanner.cpp \
    $$PWD/SAKCommonCrcInterface.cc \
    $$PWD/SAKCommonDataStructure.cc \
    $$PWD/SAKCommonInterface.cc \
    $$PWD/SAKCrcInterface.cc \
    $$PWD/SAKDataStructure.cc \
    $$PWD/SAKHighlighter.cc \
    $$PWD/SAKInterface.cc \
    $$PWD/SAKNetworkInterfaceScanner.cc \
    $$PWD/SAKObj.cc \
    $$PWD/SAKSerialPortScanner.cc \
    $$PWD/SAKSettings.cc \
    $$PWD/SAKSigleton.cc \
    $$PWD/SAKTableModel.cc \
    $$PWD/SAKTranslator.cc
