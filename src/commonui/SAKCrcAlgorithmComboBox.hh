/******************************************************************************
 * Copyright 2023 Qsaker(wuuhaii@outlook.com). All rights reserved.
 *
 * The file is encoded using "utf8 with bom", it is a part
 * of QtSwissArmyKnife project.
 *
 * QtSwissArmyKnife is licensed according to the terms in
 * the file LICENCE in the root of the source code directory.
 *****************************************************************************/
#ifndef SAKCRCALGORITHMCOMBOBOX_HH
#define SAKCRCALGORITHMCOMBOBOX_HH

#include "SAKComboBox.hh"

class SAKCrcAlgorithmComboBox : public SAKComboBox
{
    Q_OBJECT
public:
    SAKCrcAlgorithmComboBox(QWidget *parent = nullptr);
};

#endif // SAKCRCALGORITHMCOMBOBOX_HH
