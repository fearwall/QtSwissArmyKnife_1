/******************************************************************************
 * Copyright 2023 Qsaker(wuuhaii@outlook.com). All rights reserved.
 *
 * The file is encoded using "utf8 with bom", it is a part
 * of QtSwissArmyKnife project.
 *
 * QtSwissArmyKnife is licensed according to the terms in
 * the file LICENCE in the root of the source code directory.
 *****************************************************************************/
#ifndef SAKRESPONSEOPTIONCOMBOBOX_HH
#define SAKRESPONSEOPTIONCOMBOBOX_HH

#include "SAKComboBox.hh"

class SAKResponseOptionComboBox : public SAKComboBox
{
public:
    SAKResponseOptionComboBox(QWidget *parent = nullptr);
};

#endif // SAKRESPONSEOPTIONCOMBOBOX_HH
